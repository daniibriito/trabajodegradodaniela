
/////////////////////////////////////////////////////////////////////////////
//                                  LIBRERIAS                              //
/////////////////////////////////////////////////////////////////////////////

#include "DHT.h"                       // Se incluye la librería del DHT11.
#include <WiFi.h>                      // Se incluye la librería para el WIFI.
#include <HTTPClient.h>                // Se incluye la libreria de http.
#include <Arduino_JSON.h>              // Se incluye la libreria de json.


/////////////////////////////////////////////////////////////////////////////
//             DEFINIENDO NUMEROS DE PINES  Y CONSTANTES                   //
/////////////////////////////////////////////////////////////////////////////

#define timeSeconds 1                 //pir
#define sensordht 26                  //Entrada donde se conecta el DHT11.
const int motionSensor = 14;          //Entrada donde se conecta el sensor de movimiento.
const int sensor_calor = 33;          //Entrada donde se conecta el sensor de calor.
const int sensor_humo = 25;           //Entrada donde se conecta el sensor de humo.


const int power_sensor =  4;          //Salida donde se conecta el relé de los sensores en caso de incendio.
const int bomba =  13;                //Salida donde se conecta el relé para abrir las bombas en caso de incendio.
const int parlante =  2;             //Salida donde se conecta el parlante cuando se activa en caso de incendio.
const int luz =  15;                  //Salida donde se conecta la luz estroboscópica cuando se activa en caso de incendio.
const int ledazul = 19;               //Salida donde el ledazul se activa cuando detecta movimiento.
const int ledrojo =  22;              //Salida donde se conecta el Led rojo que se activa en caso de incendio. 
const int ledverde = 23;              //Salida donde se conecta el Led verde que se mantendrá encendido cuando no se registre aviso de incendio. 

const char* ssid = "TicktapsEAP";     //SSID de nuestro router.
const char* password = "software";    //Contraseña de nuestro router.

                                      //Se inserta el URL del dominio.
const char* serverName = "https://apidimraw.myticktap.com/api/1.0/alarma/monitor/20";


unsigned long lastTime = 0;

                                      // Se esperan 5 segundos.
unsigned long timerDelay = 5000;


float temperature = 0.0f;
float humidity = 0.0f;

String si = "SI";
String no = "NO";


String sensorReadings;
float sensorReadingsArr[3];

DHT dht1(sensordht, DHT11);           //Sensor DHT11
int sensorState = 0;                  //Variable de almacenamiento para el sensor de calor. 
int sensorState2 = 0;                 //Variable de almacenamiento para el sensor de humo. 
int salida = 5;
int enviarAlarma = 0;
unsigned long alarmaActivada = 0;


float t1 = 0.0f;
float h1 = 0.0f;


/////////////////////////////////////////////////////////////////////////////
//                          Variables para el GET                          //
/////////////////////////////////////////////////////////////////////////////

int sensorhumoValue;
int sensorcalorValue;
int sensormovimientoValue;
int numid=20;
float sensortemperaturaValue;
float sensorhumedadValue;

String nameid = "&id_alarma=";
String namehumo = "&humo=";
String namecalor = "&calor=";
String nametemperatura = "&temperatura=";
String namehumedad = "&humedad=";
String namemovimiento = "&movimiento=";
String namedate = "&date=";
boolean control = true;

String serverName2 =  "https://apidimraw.myticktap.com/api/1.0/alarma/monitor/guardar?";
unsigned long lastTime2 = 0;
unsigned long timerDelay2 = 30000;
HTTPClient http;




                                       
unsigned long now = millis();
unsigned long lastTrigger = 0;
boolean startTimer = false;
boolean startTimer2 = true;

                                      // Comprueba si se detectó movimiento, enciende el led azul e inicia un temporizador
/////////////////////////////////////////////////////////////////////////////
//                                 PARTE 1 MOVIMIENTO                      //
/////////////////////////////////////////////////////////////////////////////

void  IRAM_ATTR detectsMovement() {
  
  sensormovimientoValue=1;
  Serial.println(sensormovimientoValue);
  Serial.println("ESTADO DEL SENSOR DE MOVIMIENTO: SE DETECTA MOVIMIENTO!");
  digitalWrite(ledazul, HIGH);
 
  startTimer = true;
  startTimer2 = false;
  lastTrigger = millis();
}

void setup() {
  Serial.begin(9600);
  WiFi.begin(ssid,password); 
  Serial.println("Conectando al Wifi"); //Informa en el monitor serie4 si se esta conectando el dispositivo al WIFI.

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  
  Serial.println("\nConectado al WIFI.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Timer set to 10 seconds (timerDelay variable), it will take 10 seconds before publishing the first reading.");
  
  pinMode(ledrojo, OUTPUT);           // Inicializa ledrojo como salida.
  pinMode(ledverde, OUTPUT);          // Inicializa ledverde como salida.
  pinMode(parlante, OUTPUT);          // Inicializa luz como salida.
  pinMode(luz, OUTPUT);               // Inicializa luz como salida.
  pinMode(power_sensor, OUTPUT);      // Inicializa power_sensor como salida.
  pinMode(bomba, OUTPUT);             // Inicializa power_sensor como salida.
  
  pinMode(sensor_calor, INPUT);       // Inicializa el botón como entrada.
  pinMode(sensor_humo, INPUT);        // Inicializa el botón como entrada.
  
  digitalWrite(ledrojo, LOW);          // Inicializa ledrojo como bajo.
  digitalWrite(parlante, LOW);         // Inicializa el parlante como bajo.
  digitalWrite(luz, LOW);              // Inicializa luz como bajo.
  digitalWrite(power_sensor, LOW);     // Inicializa power_sensor como bajo.
  digitalWrite(bomba, LOW);            // Inicializa power_sensor como bajo.

  digitalWrite(ledverde, HIGH);        // Inicializa el ledverde como alto.
    
   // PIR   modo INPUT_PULLUP
  pinMode(motionSensor, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(motionSensor), detectsMovement, RISING);



 //  LED a LOW
  pinMode(salida, OUTPUT);
  digitalWrite(salida, LOW);

  // LED a HIGH
  pinMode(ledazul, OUTPUT);
  digitalWrite(ledazul, LOW);
  
  Serial.println("Test de sensores:");//Escribe en monitor serie los valores del DHT11.
  Serial.println("Por favor espere 5 segundos para recibir la lectura del JSON.");
  dht1.begin();       
  //sendTemperatureToNextion();
  //sendTermicoToNextion();
  //sendHumidityToNextion();
  //sendHumoToNextion();
  //sendMovimientoToNextion() ;               
}

/////////////////////////////////////////////////////////////////////////////
//               CIRCULO DONDE SE REALIZARÁ CADA FUNCIÓN                   //
/////////////////////////////////////////////////////////////////////////////
void loop() {
   
  
  conexionwifi();
  //leergetjson();
  detectarmovimiento2();
  leersensordht11();
  activaralarmaincendiotermico();
  activaralarmaincendiohumo();
  mostrarValoresPantalla();
  apagarSirena();
  //delay(5000);
}

/////////////////////////////////////////////////////////////////////////////
//                           CONEXIÓN WIFI                                 //  
/////////////////////////////////////////////////////////////////////////////


void conexionwifi() {
if ((millis() - lastTime2) > timerDelay2) {
  
     if ((WiFi.status() == WL_CONNECTED)) {
    Serial.println("Conectado");
    
    
    
       
    String serverPath = "https://apidimraw.myticktap.com/api/1.0/alarma/monitor/guardar?&id_alarma=" + String(numid) + "&humo="+String(sensorhumoValue) +
    "&calor="+String(sensorcalorValue) + "&temperatura="+String(sensortemperaturaValue)+  "&humedad="+String(sensorhumedadValue)
    + "&movimiento="+String(sensormovimientoValue) + "&date="+String(millis());

     Serial.println(serverPath);
      // Your Domain name with URL path or IP address with path
      http.begin(serverPath.c_str());
      
      // Send HTTP GET request
      int httpResponseCode = http.GET();
      
      if (httpResponseCode>0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        String payload = http.getString();
        Serial.println(payload);
      }
      else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
      }
      // Free resources
      http.end();
    }
    
   else {
      Serial.println("WiFi Disconnected");
    }
    lastTime2 = millis();
  }
 
  
}
/////////////////////////////////////////////////////////////////////////////
//                  SENSOR DE TEMPERATURA Y HUMEDAD DHT11                  //  
/////////////////////////////////////////////////////////////////////////////

void leersensordht11() {
  
  t1 = dht1.readTemperature();          //Guarda el valor de la temperatura leida por el DHT11 en t1.
  h1 = dht1.readHumidity();             //Guarda el valor de la temperatura leida por el DHT11 en h1.

  while (isnan(t1) || isnan(h1)){             //Ciclo mientras lee los valores y los encuentra.
    Serial.println("Lectura fallida en el sensor DHT11, repitiendo lectura...");
    delay(2000);
    t1 = dht1.readTemperature();
    h1 = dht1.readHumidity();
    temperature = t1;
    humidity = h1;

   
  }

  Serial.print("Temperatura del DHT11: ");     //Muestra el valor de temperatura del DHT11 en el monitor serie.
  Serial.print(t1);
  Serial.println(" ºC.");
  sensortemperaturaValue = t1;

  Serial.print("Humedad del DHT11: ");        //Muestra el valor de humedad del DHT11 en el monitor serie.
  Serial.print(h1);
  Serial.println(" %."); 
  sensorhumedadValue = h1;

//sendHumidityToNextion();
//sendTemperatureToNextion();
//endNextionCommand();
  Serial.println("-----------------------");  //Se reinicia el ciclo para mostrar los nuevos valores que detecte.
}

void mostrarValoresPantalla(){
  String command;
  refrescarPantalla();
  if (sensorcalorValue == LOW)
  command = "termico.txt=\""+no+"\"";
  else
  command = "termico.txt=\""+si+"\"";
  Serial.print(command);
  refrescarPantalla();
  if (sensorhumoValue == LOW)
    command = "humo.txt=\""+no+"\"";
    else
    command = "humo.txt=\""+si+"\"";
  Serial.print(command);
  refrescarPantalla();
   command = "humidity.txt=\""+String(h1)+"\"";
  Serial.print(command);
  refrescarPantalla();
    command = "temperature.txt=\""+String(t1)+"\"";
  Serial.print(command);
  refrescarPantalla();
  if(sensormovimientoValue==1)
  command = "movimientop.txt=\""+si+"\"";
  else if (sensormovimientoValue==0)
  command = "movimientop.txt=\""+no+"\"";
  Serial.print(command);
  refrescarPantalla();
  }


  void refrescarPantalla()
{
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
}



/////////////////////////////////////////////////////////////////////////////
//                      SENSOR DE MOVIMIENTO                               //  
/////////////////////////////////////////////////////////////////////////////

void detectarmovimiento2(){
   // Current time
  now = millis();
  // Turn off the LED after the number of seconds defined in the timeSeconds variable
  if(startTimer && (now - lastTrigger > (timeSeconds*1000))) {
  sensormovimientoValue=0;
  Serial.println(sensormovimientoValue);
    Serial.println("Movimiento NO detectado");
    digitalWrite(ledazul, LOW);
    startTimer2 = false;
  }
}


/////////////////////////////////////////////////////////////////////////////
//                ACTIVAR ALARMA CONTRA INCENDIOS  CALOR                   //  
/////////////////////////////////////////////////////////////////////////////
void activaralarmaincendiotermico() {
                                           
  sensorState = digitalRead(sensor_calor);   // Lee el estado del valor en que se encuentre el sensor de calor y almacena en esa variable.

  
  digitalWrite(power_sensor, HIGH);
  delay (300);


                                            // El if comprueba el estado del sensorState, el detector de calor se activa en bajo y se apaga en alto.
  if (sensorState == HIGH) {                // Por lo que si  el estado del sensorState es alto 
 
    digitalWrite(ledrojo, LOW);              //El led rojo que avisa sobre un incendio se mantendrá apagado.
    digitalWrite(ledverde, HIGH);           //Se mantendrá encendido el led verde indicando que no se ha detectado incendio.
   if (alarmaActivada==0){
    digitalWrite(parlante, LOW);            //El parlante se mantendrá apagada.
    digitalWrite(bomba, LOW);               //La bomba se mantendrá cerrada.
    digitalWrite(luz, LOW);                 //La luz estroboscópica se mantendrá apagada.
    }
    
    Serial.println("ESTADO DEL AVISO DE INCENDIOS: DESACTIVADO"); // Se mostrará en el monitor serie que el aviso de incendios se encuentra desactivado.
  } else {                                  // Si no
    
    digitalWrite(power_sensor, LOW);        //Se activará el sensor de calor reconociendo la presencia de un incendio.
    digitalWrite(ledverde, LOW);            //Se apagará el led verde.
    digitalWrite(ledrojo, HIGH);            //El led rojo que avisa sobre un incendio se encenderá avisando un incendio de forma visual en la central física.
    digitalWrite(parlante, HIGH);             //La alarma se encenderá avisando un incendio de forma sonora en la ubicación del sistema de alarma.
    digitalWrite(bomba, HIGH);               //La bomba se abrirá.
    digitalWrite(luz, HIGH);                //La luz estroboscópica se encenderá avisando un incendio de forma visual en la ubicación del sistema de alarma.
    Serial.println("ESTADO DEL AVISO DE INCENDIOS: ACTIVADO"); // Se mostrará en el monitor serie que el aviso de incendios se encuentra activado.
    enviarAlarma =0;
    sensorcalorValue = 1;
    alarmaActivada = millis();
    delay (50);                          //Pasan 10 segundos.     
    digitalWrite(power_sensor, HIGH);       //Y el sensor pasará a alto, apagando el aviso automáticamente.
  
 
  }
}

/////////////////////////////////////////////////////////////////////////////
//                ACTIVAR ALARMA CONTRA INCENDIOS  HUMO                   //  
/////////////////////////////////////////////////////////////////////////////
void activaralarmaincendiohumo() {
                                           
  sensorState2 = digitalRead(sensor_humo);    // Lee el estado del valor en que se encuentre el sensor de humo y almacena en esa variable.

  
  digitalWrite(power_sensor, HIGH);
  delay (300);


                                            // El if comprueba el estado del sensorState, el detector de calor se activa en bajo y se apaga en alto.
  if (sensorState2 == HIGH) {                // Por lo que si  el estado del sensorState es alto 
 
    digitalWrite(ledrojo, LOW);              //El led rojo que avisa sobre un incendio se mantendrá apagado.
    digitalWrite(ledverde, HIGH);           //Se mantendrá encendido el led verde indicando que no se ha detectado incendio.
    if (alarmaActivada==0){
    digitalWrite(parlante, LOW);            //El parlante se mantendrá apagada.
    digitalWrite(bomba, LOW);               //La bomba se mantendrá cerrada.
    digitalWrite(luz, LOW);                 //La luz estroboscópica se mantendrá apagada.
    }
    
    //Serial.println("ESTADO DEL AVISO DE INCENDIOS: DESACTIVADO"); // Se mostrará en el monitor serie que el aviso de incendios se encuentra desactivado.
  } else {                                  // Si no
    
    digitalWrite(power_sensor, LOW);        //Se activará el sensor de humo reconociendo la presencia de un incendio.
    digitalWrite(ledverde, LOW);            //Se apagará el led verde.
    digitalWrite(ledrojo, HIGH);            //El led rojo que avisa sobre un incendio se encenderá avisando un incendio de forma visual en la central física.
    digitalWrite(parlante, HIGH);             //La alarma se encenderá avisando un incendio de forma sonora en la ubicación del sistema de alarma.
    digitalWrite(bomba, HIGH);               //La bomba se abrirá.
    digitalWrite(luz, HIGH);                //La luz estroboscópica se encenderá avisando un incendio de forma visual en la ubicación del sistema de alarma.
    Serial.println("ESTADO DEL AVISO DE INCENDIOS: ACTIVADO"); // Se mostrará en el monitor serie que el aviso de incendios se encuentra activado.
    enviarAlarma =0;
    alarmaActivada = millis();
    sensorhumoValue = 1;
    delay (50);                          //Pasan 10 segundos.     
    digitalWrite(power_sensor, HIGH);       //Y el sensor pasará a alto, apagando el aviso automáticamente.
  
 
  }
}


void apagarSirena(){
  if (enviarAlarma==0)
  {
    lastTime2=0;
    enviarAlarma=1;
    }
    if ((millis() - alarmaActivada)>30000){
   
    digitalWrite(parlante, LOW);            //El parlante se mantendrá apagada.
    digitalWrite(bomba, LOW);               //La bomba se mantendrá cerrada.
    digitalWrite(luz, LOW);                 //La luz estroboscópica se mantendrá apagada.
    alarmaActivada = 0;
    }  
  }

/////////////////////////////////////////////////////////////////////////////
//            GET PARA LEER EL JSON DE LA API DEL SERVIDOR                 //  
/////////////////////////////////////////////////////////////////////////////

void leergetjson()  {
  if ((millis() - lastTime) > timerDelay) {
    //Chequea el estatus de la conexion con el WIFI.
    if(WiFi.status()== WL_CONNECTED){
              
      sensorReadings = httpGETRequest(serverName);
      Serial.println(sensorReadings);
      JSONVar myObject = JSON.parse(sensorReadings);

      if (JSON.typeof(myObject) == "undefined") {
        Serial.println("Parsing input failed!");
        return;
      }
    
      Serial.print("JSON object = ");
      Serial.println(myObject);
    
      JSONVar keys = myObject.keys();
 
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}

String httpGETRequest(const char* serverName) {
  HTTPClient http;
    

  http.begin(serverName);
  

  int httpResponseCode = http.GET();
  
  String payload = "{}"; 
  
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }

  http.end();

  return payload;
}
